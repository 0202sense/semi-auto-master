export function setup(ctx) {
    ctx.onCharacterSelectionLoaded(ctx => {
        // debug
        const debugLog = (...msg) => {
            mod.api.SEMI.log(`${id} v4645`, ...msg);
        };

        // script details
        const id = 'semi-auto-master';
        const title = 'SEMI Auto Master';

        // setting groups
        const SETTING_GENERAL = ctx.settings.section('General');
        const SETTING_SKILLS = ctx.settings.section('Per Skill Toggles');
        const SETTING_TOKENS = ctx.settings.section('Per Skill Mastery Token Toggles');

        // variables
        let MAX_ATTEMPTS = 10;

        // utils
        const htmlID = (id) => id.replace(/[:_]/g, '-').toLowerCase();

        // notifications
        let _notificationNuked = false;
        const _fireBottomToast = fireBottomToast;
        fireBottomToast = function(...args) { if (!_notificationNuked) { _fireBottomToast(...args);}};
        const _fireTopToast = fireTopToast;
        fireTopToast = function(...args) { if (!_notificationNuked) { _fireTopToast(...args);}};

        const toggleMasteryConfirm = (wasEnabled) => {
            if (wasEnabled) {
                game.settings.toggleSetting('showMasteryCheckpointconfirmations')
            }
        };

        // Mastery Value Cache
        let masterySkillAlt = 0;
        const masterySkills = game.masterySkills.filter(skill => skill.hasMastery);
        const masterySkillsCap = masterySkills.length;
        const masteryTokenXP = {};
        const masteryPoolXP = {}; // XP for generally 95% pool.

        masterySkills.forEach(skill => {
            masteryTokenXP[skill.id] = Math.floor((skill.baseMasteryPoolCap * skill.masteryToken.modifiers.masteryToken[0].value) / 100);
            masteryPoolXP[skill.id] = skill.baseMasteryPoolCap * (masteryCheckpoints[masteryCheckpoints.length - 1] / 100);
        });

        // Mastery Actions
        const spendTokens = (skill, qty) => game.bank.claimItemOnClick(skill.masteryToken, qty);
        const actionXPCost = (skill, action) => exp.level_to_xp(skill.getMasteryLevel(action) + 1) + 1 - skill.getMasteryXP(action);

        /**
        * Get the lowest mastery action, excluding masteries equal to or above the mastery level cap.
        */
        const lowestMastery = (skill) => {
            const skillActions = skill.sortedMasteryActions;
            const masteryCap = skill.masteryLevelCap; // Normally 99

            let lowestAction;
            let lowestLevel = masteryCap;

            for (let n = 0; n < skillActions.length; n++) {
                let actionMastery = skill.getMasteryLevel(skillActions[n]);

                if (actionMastery >= masteryCap)
                    continue;

                if (actionMastery < lowestLevel) {
                    lowestAction = skillActions[n];
                    lowestLevel = actionMastery;

                    if (lowestLevel <= 1)
                        break;
                }
            }

            return lowestAction;
        };

        /**
        * Check whether we should spend pool XP to upgrade a Mastery Action.
        This take into account bonus XP claimable from tokens if provided.
        */
        const canSpendXP = (skill, action, bonusXP = 0) => {
            // Invalid Pool Values
            if (masteryPoolXP[skill.id] == null || masteryPoolXP[skill.id] <= 0)
                return false;

            // Under final checkpoint XP value.
            if (skill.masteryPoolXP < masteryPoolXP[skill.id])
                return false;

            // Pool is Capped
            if (skill.masteryPoolXP === skill.masteryPoolCap)
                return true;

            const xpCost = actionXPCost(skill, action);

            // No enough XP to spend.
            if (skill.masteryPoolXP < xpCost)
                return false;

            // Spending would put under checkpoint, including bonus XP from tokens.
            if (skill.masteryPoolXP - xpCost + bonusXP < masteryPoolXP[skill.id])
                return false;

            return true;
        };

        /**
        * Handle a skills mastery upgrades.
        */
        const handleSkill = (skill) => {
            // No Mastery
            if (!skill.hasMastery)
                return;

            // No Mastery to Level
            let initialMasteryAction = lowestMastery(skill);
            if (!initialMasteryAction)
                return;

            // Get Settings
            const autoMasterEnabled = SETTING_GENERAL.get(`${id}-enable`);
            const autoMasterSkillEnabled = SETTING_SKILLS.get(`skill-${htmlID(skill.id)}`);
            const canUseTokens = SETTING_GENERAL.get(`enable-tokens`) && SETTING_TOKENS.get(`token-${htmlID(skill.id)}`);

            if (!autoMasterEnabled || !autoMasterSkillEnabled) {
                //debugLog('autoMasterEnabled:', autoMasterEnabled, 'autoMasterSkillEnabled:', autoMasterSkillEnabled);
                return;
            }

            const allButOneIsEnabled = mod.api.SEMI.getSubmodSettings('SEMI All But One', 'all-but-one');
            const displayNotificationsSetting = game.settings.showMasteryCheckpointconfirmations;

            const tokenCount = (skill) => {
                if (!canUseTokens)
                    return 0;

                return Math.max(0, (game.bank.getQty(skill.masteryToken) - (allButOneIsEnabled ? 1 : 0)));
            };

            const spendableTokens = (skill) => {
                const tokenQty = tokenCount(skill);
                if (tokenQty > 0) {
                    const xpPerToken = masteryTokenXP[skill.id];
                    const xpRemaining = skill.masteryPoolCap - skill.masteryPoolXP;
                    const tokensToFillPool = Math.floor(xpRemaining / xpPerToken);
                    return Math.min(tokenQty, tokensToFillPool);
                }
                return 0;
            }

            // Blackhole NotificationQueue and Toastify
            _notificationNuked = true;

            // Use Tokens, check for final pool checkpoint.
            const bankTokens = spendableTokens(skill);
            if (bankTokens > 0) {
                spendTokens(skill, bankTokens);
            }

            const bankTokenXP = masteryTokenXP[skill.id] * tokenCount(skill);
            // Can't afford initial mastery action.
            if (!canSpendXP(skill, initialMasteryAction, bankTokenXP)) {
                _notificationNuked = false;
                return;
            }

            // Begin Mastery Handling
            //const startTime = performance.now();
            //debugLog(`[${skill.name}] Beginning mastery spending`);

            toggleMasteryConfirm(displayNotificationsSetting);

            // Level Up multiple masteries.
            for (let n = 1; n <= MAX_ATTEMPTS; n++) {
                let tokenQty;
                let postSpendTokens = 0;
                let bestMastery = lowestMastery(skill);

                if (!bestMastery) {
                    debugLog(`[${skill.name}] No more master actions to level up, finished in ${n} loops.`);
                    break;
                }

                let canAfford = canSpendXP(skill, bestMastery);

                // Cannot afford, use tokens and check again.
                if (!canAfford) {
                    tokenQty = spendableTokens(skill);
                    if (tokenQty > 0) {
                        spendTokens(skill, tokenQty);
                        canAfford = canSpendXP(skill, bestMastery);
                    }
                }

                // Can't afford but would have enough tokens to restore pool.
                if (!canAfford) {
                    tokenQty = tokenCount(skill);
                    if (tokenQty > 0) {
                        let maxTokenXP = masteryTokenXP[skill.id] * tokenQty;
                        let actionCost = actionXPCost(skill, bestMastery);

                        if (skill.masteryPoolXP - actionCost + maxTokenXP > masteryPoolXP[skill.id]) {
                            postSpendTokens = tokenQty;
                            canAfford = true;
                        }
                    }
                }

                // Still can't afford, bail.
                if (!canAfford) {
                    debugLog(`[${skill.name}] Not enough mastery XP / tokens to afford upgrade.`);
                    break;
                }

                // Level Up
                skill.levelUpMasteryWithPoolXP(bestMastery, 1);

                // Refill Pool
                if (postSpendTokens > 0) {
                    spendTokens(skill, spendableTokens(skill));
                }
            }

            // Restore NotificationQueue and Toastify
            toggleMasteryConfirm(displayNotificationsSetting);
            _notificationNuked = false;

            //const endTime = performance.now();
            //debugLog(`[${skill.name}] Mastery spending complete, took ${endTime - startTime}ms.`);
        };

        // settings
        SETTING_GENERAL.add([{
            'type': 'switch',
            'name': `${id}-enable`,
            'label': `Enable ${title}`,
            'default': true
        }, {
            'type': 'switch',
            'name': `enable-tokens`,
            'label': `Use Mastery Tokens`,
            'default': true
        }]);

        SETTING_SKILLS.add(
            masterySkills.map(skill => {
                return {
                    'type': 'switch',
                    'name': `skill-${htmlID(skill.id)}`,
                    'label': $(`<span><img src="${skill.media}" class="resize-24"> Enable ${skill.name}</span>`).get(0),
                    'default': true
                }
            })
        );

        SETTING_TOKENS.add(
            masterySkills.map(skill => {
                return {
                    'type': 'switch',
                    'name': `token-${htmlID(skill.id)}`,
                    'label': $(`<span><img src="${skill.masteryToken.media}" class="resize-24"> Enable ${skill.masteryToken.name}</span>`).get(0),
                    'default': true
                }
            })
        );

        // game hooks
        let _masteryNotificationSetting;

        ctx.patch(NotificationQueue, 'add').replace(function(o, notification) {
            if (!_notificationNuked) {
                o(notification);
            }
        });

        ctx.onCharacterLoaded(ctx => {
            _masteryNotificationSetting = game.settings.showMasteryCheckpointconfirmations;
            toggleMasteryConfirm(_masteryNotificationSetting);

            ctx.patch(Skill, 'addXP').after(function(result, xpAmount, masteryAction) {
                handleSkill(this);

                // Handle a looping other skill.
                handleSkill(masterySkills[masterySkillAlt]);
                masterySkillAlt++;
                if (masterySkillAlt >= masterySkillsCap) {
                    masterySkillAlt = 0;
                }
            });
        });

        ctx.onInterfaceReady(ctx => {
            toggleMasteryConfirm(_masteryNotificationSetting);
            MAX_ATTEMPTS = 50;
        });
    });
}
